const express 			= require('express');
const router 			= express.Router();


const ProductsController    = require('./../controllers/ProductsController');

const path  = require('path');

// Products API
router.get('/products', ProductsController.index);
router.post('/products', ProductsController.create);
router.post('/import', ProductsController.importcsv);

module.exports = router;