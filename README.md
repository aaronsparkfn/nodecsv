# Microservice Practice Import CSV

### Dependencies
1. NodeJS
2. Express
3. Sequelize
4. Express Uploader
5. CSV

### Config

 - config/config.json change the development credentials
 
### Installation
```sh
$ cd nodecsv
$ npm install
$ sequelize db:migrate
$ npm start // Start Server
```

