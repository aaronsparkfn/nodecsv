'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Products', [{
      name : 'Raven Mask',
      description : 'I am a new user to this application',
      quantity: 10,
      createdAt : new Date(),
      updatedAt : new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('Products', [{
      name :'Raven Mask'
    }])
  }
};
