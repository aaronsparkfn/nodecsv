const Product = require('../models').Product;

/* CSV */
const fs    = require('fs');
const csv   = require('csv');

const index = function(req, res) {
    Product.findAll()
		.then(function (products) {
			res.json(products);
		});
}

module.exports.index = index;

const create = function(req, res) {
    
    Product.create({
            sku: req.body.sku,
            name: req.body.name,
            description: req.body.description,
            quantity: req.body.quantity
        }).then(product => {
            res.status(200)
                .json(product);
        }).catch(err => {
            res.status(422).json(err);
        });
}

module.exports.create = create;

const importcsv = function(req, res) {
    if (!req.files)
            return res.status(400).send('No files were uploaded.');
        
        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        let sampleFile = req.files.sampleFile;
        
        // Use the mv() method to place the file somewhere on your server
        sampleFile.mv(appRoot + '/csv/'+req.files.sampleFile.name, function(err) {
            if (err)
            return res.status(500).send(err);
        
            var input = fs.createReadStream('./csv/'+ req.files.sampleFile.name);
              var parser = csv.parse({
                columns: true
              });

              var transform = csv.transform(function (row) {
                    var obj = {
                        sku: row['sku'],
                        name: row['name'],
                        description: row['description'],
                        quantity: row['quantity']
                    }

                    Product.create(obj)
                        .then(result => {
                            console.log('Records Created!');
                        })
                        .catch(error => {
                            console.log(error);
                        });  
              });
             
              input.pipe(parser).pipe(transform);
            //res.send('File uploaded!');
            res.json({ message: 'CSV Successfully imported!'});
        });
}

module.exports.importcsv = importcsv;