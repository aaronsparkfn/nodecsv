const chai = require('chai');
const assert = chai.assert;
const expect = chai.expect;
const chaiHttp = require('chai-http');
const Product = require('../models').Product;

chai.use(chaiHttp);

const app = require('../app.js');

describe('Products Controller', () => {
    before((done) => {
        Product.destroy({ truncate: true });
        done();
    });

    it('Create product', (done) => {
        let product = {
            sku: '23',
            name: 'Test Product',
            description: 'Lorem ipsum dolor asmet',
            quantity: 23
        };

        chai.request(app)
            .post('/api/products')
            .send(product)
            .then(res => {
                //assert.equal(product, response.body);
                assert.isObject(res, 'Check if its object');
                //assert.includeDeepMembers(product, response, 'Check Objects');
                expect(res).to.have.status(200);
                expect(res.type).to.eql('application/json');
                done();
            }).catch(err => {
                done(err);
            });
    });

    it('Fetch all products', (done) => {
        chai.request(app)
            .get('/api/products')
            .end((err, res) => {
                if (err) done(err);
                // Now let's check our response
                expect(res).to.have.status(200);
                expect(res).to.be.a('object');
                expect(res.type).to.eql('application/json');
                done();
            });
    });

});