const express 		= require('express');
const logger 	    = require('morgan');
const fileUpload  = require('express-fileupload');
const path          = require('path');

const bodyParser 	= require('body-parser');
const jsonParser    = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false })

global.appRoot    = path.resolve(__dirname);
// const bodyParser 	= require('body-parser');
// const jsonparse   = bodyParser.json();
//const multer  = require('multer')
//const upload = multer({ dest: 'uploads/' })

const api = require('./routes/api');

const app = express();

app.use(logger('dev'));
app.use(fileUpload());
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({ extended: false }));
;
// Database
const models = require("./models");
models.sequelize.authenticate().then(() => {
    console.log('Connected to SQL database:');
})
.catch(err => {
    console.error('Unable to connect to SQL database:', err);
});


app.use('/api', jsonParser, api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;